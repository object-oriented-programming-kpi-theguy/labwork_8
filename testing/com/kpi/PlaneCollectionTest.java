package com.kpi;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Yuriy on 17.06.2016.
 */
public class PlaneCollectionTest {
    private PlaneCollection myPlanes;

    @Before
    public void initialize(){
        PlaneCollection myPlanes = new PlaneCollection();
        myPlanes.add(new Planes(400, 606.3, 455));
        myPlanes.add(new Boeing(600, 405.4, 788));
        myPlanes.add(new An(245, 808.4, 455));
        myPlanes.add(new Airbus(606, 408.8, 233));
        myPlanes.add(new Boeing());
    }

    @Test
    public void isEmpty() throws Exception {
        Assert.assertFalse(myPlanes.isEmpty());
    }

    @Test
    public void toArray() throws Exception {

    }

    @Test
    public void add() throws Exception {

    }

    @Test
    public void remove() throws Exception {

    }

    @Test
    public void remove1() throws Exception {

    }

    @Test
    public void subList() throws Exception {

    }

}
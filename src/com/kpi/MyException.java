package com.kpi;

/**
 * Created by Yuriy on 17.06.2016.
 */
public class MyException extends Exception {
    private int n;

    public void SetN(int N){
        this.n = N;
    }

    public MyException(int except){
        SetN(except);
    }

    public String toString(){
        String txt = "Planes must be lover than 3! You entered:" + n;
        return txt;
    }
}
